from django.urls import path
from . import views

app_name = 'django_practice'
urlpatterns = [
	path('', views.index,name= 'index'),
	path('register',views.register, name='register'),
	path('change_password',views.change_password, name="change_password"),
	path('login', views.login_view, name="login"),
	path('logout', views.logout_view, name="logout"),
	path('add_item', views.add_item, name="add_item"),
	path('<int:groceryitem_id>/', views.groceryitem, name = 'viewgroceryitem'),
	path('<int:groceryitem_id>/edit', views.update_item, name='update_item'),
	path('<int:groceryitem_id>/delete', views.delete_item, name='delete_item'),

]

