from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login,logout
from django.forms.models import model_to_dict
from django.utils import timezone
# Create your views here.

from .models import GroceryItem
from .forms import LoginForm, AddItemForm, UpdateItemForm, RegisterForm


def index(request):
	groceryitem_list = GroceryItem.objects.filter(user_id = request.user.id)
	context = {
		'groceryitem_list': groceryitem_list,
		'user': request.user
	}
	return render(request, "django_practice/index.html",context)

def groceryitem(request,groceryitem_id):
	groceryitem = get_object_or_404(GroceryItem,pk=groceryitem_id)

	return render (request, "django_practice/groceryitem.html", model_to_dict(groceryitem))


def register(request):
	
	context = {}
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid() == False:
			form = RegisterForm()
		else:
			user = User()
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			is_staff = False
			is_active = True 

			# user = authenticate(username=username, password=password)
			# print(user)
			# if user is None:
			# 	authenticated_user = User(username = username)
			# 	authenticated_user.set_password(password)
			# 	authenticated_user.save()

			

			duplicates = User.objects.filter(username = username)

			if not duplicates:
				
				User.objects.create(username = username, first_name = first_name, last_name = last_name, email = email, is_staff = is_staff , is_active = is_active , password = password)

				
				return redirect("django_practice:login")
			else:
				context = {
					"error": True
				}
	return render(request, "django_practice/register.html", context)


def change_password(request):
	is_user_authenticated = False

	user = authenticate(username="alexbuena", password="alexa1")
	print(user)

	if user is None:
		authenticated_user = User.objects.get(username="johndoe")
		authenticated_user.set_password('johndoe1')
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		"is_user_authenticated": is_user_authenticated
	}

	return render(request, "django_practice/change_password.html", context)

def login_view(request):
	context = {}
	
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid() == False:
			form = LoginForm()
		else:

			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			# authenticated_user = User(username = username)
			# authenticated_user.set_password(password)
			# authenticated_user.save()
			user = authenticate(username=username, password=password)
			print(user)
				
			context = {
				"username": username,
				"password": password
			}
	
			if user is not None:
				login(request,user)
				return redirect("django_practice:index")
			else:
				context = {
					"error": True
				}
	return render(request, "django_practice/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("django_practice:login")

def add_item(request):
	context = {}
	if request.method == 'POST':
		form = AddItemForm(request.POST)
		if form.is_valid() == False:
			form = AddItemForm()
		else:
			item_name = form.cleaned_data['item_name']
			category = form.cleaned_data['category']

			duplicates = GroceryItem.objects.filter(item_name = item_name)

			if not duplicates:
				GroceryItem.objects.create(item_name = item_name, category = category, date_created = timezone.now(),user_id=request.user.id)
				return redirect("django_practice:index")
			else:
				context = {
					"error": True
				}
	return render(request, "django_practice/add_item.html", context)

def update_item(request, groceryitem_id):
	groceryitem = GroceryItem.objects.filter(pk=groceryitem_id)

	context ={
		"user": request.user,
		"groceryitem_id": groceryitem_id,
		"item_name": groceryitem[0].item_name,
		"category": groceryitem[0].category,
		"status": groceryitem[0].status
	}
	if request.method == 'POST':
		form = UpdateItemForm(request.POST)
		if form.is_valid() == False:
			form = UpdateItemForm()
		else:
			item_name = form.cleaned_data['item_name']
			category = form.cleaned_data['category']
			status = form.cleaned_data['status']

			if groceryitem:
				groceryitem[0].item_name = item_name
				groceryitem[0].category = category
				groceryitem[0].status = status

				groceryitem[0].save()
				return redirect("django_practice:index")

			else:
				context ={
					"error": True
				}
	return render(request, "django_practice/update_item.html", context)

def delete_item(request, groceryitem_id):
	groceryitem = GroceryItem.objects.filter(pk=groceryitem_id).delete()
	return redirect("django_practice:index")


